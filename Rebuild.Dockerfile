FROM owasp/dependency-check:12.1.0@sha256:60ee7af9cf80ac009761e397b2d4ba5ddbf072c2a0ead1c068dc24dc62155600

RUN --mount=type=secret,id=nvd,target=/kaniko/NVD_API_KEY \
    /usr/share/dependency-check/bin/dependency-check.sh \
    --updateonly --nvdApiKey "$(cat /kaniko/NVD_API_KEY)" \
    --nvdValidForHours 1 \
    --nvdApiDelay 4000

COPY suppressions/ /suppressions/

# Add bash to image per #57. Base image has unprivileged user name saved in $user
USER root
# hadolint ignore=DL3018
RUN apk update && apk add --no-cache bash && rm -rf /var/cache/apk/*
USER $user

LABEL org.opencontainers.image.licenses="Apache-2.0"
LABEL org.opencontainers.image.source="https://gitlab.com/gitlab-ci-utils/docker-dependency-check"
LABEL org.opencontainers.image.title="docker-dependency-check"
LABEL org.opencontainers.image.url="https://gitlab.com/gitlab-ci-utils/docker-dependency-check"

ENTRYPOINT ["/usr/share/dependency-check/bin/dependency-check.sh"]
CMD ["--scan","/builds","--format","ALL","--project","GENERIC","--failOnCVSS","0"]
